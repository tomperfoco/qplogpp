# list of defines

QPLOG_USE_ABORT
-----------------------------------
Crash a program, when hit log on specific level.
You can use setAbortLevel(LOGLEVEL level) function to add abort level.
For example QPL_FATAL for release version
and QPL_FATAL | QPL_ERROR | QPL_CRITICAL for debug version of program.

QPLOG_USE_FILE_LINE
-----------------------------------
Use file and line to mark position of log.

QPLOG_USE_FUNC_INFO
-----------------------------------
Use Q_FUNC_INFO macro to mark position of log.

QPLOG_USE_COLORS
-----------------------------------
Use colors. Works only with *nix console colors system.

QPLOG_USE_BENCHMARK
-----------------------------------
Use benchmark - a macro measures live time of function. You can use
qPBenchmark(someTextInfo) and qPBenchmarkCheckpoint(someTextInfo) macros.

QPLOG_USE_COMPARER
-----------------------------------
Simply compare two binary objects (not tested and not develop now).

QPLOG_USE_GUI_ABORT
-----------------------------------
Run QDialog before crash a program. If QPLOG_USE_ABORT is not defined,
QDialogue will be displayed, but program do not going crash.
