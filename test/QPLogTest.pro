# Copyright (c) 2014, Tomasz Foltyński <tom@perfoco.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any purpose
# with or without fee is hereby granted, provided that the above copyright notice and
# this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
# TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN
# NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#-------------------------------------------------
#
# Project created by QtCreator 2014-02-12T06:58:41
#
#-------------------------------------------------

QT          += testlib

#QT          -= gui # QPLOG_USE_GUI_ABORT requires a gui module
QT += widgets

TARGET       = tst_qplogtesttest
CONFIG      += console
CONFIG      -= app_bundle

TEMPLATE     = app

SOURCES     += tst_qplogtesttest.cpp \
    ../src/qplog.cpp \
    testerhelper.cpp

INCLUDES    += ../src/qplog.h \

DEFINES     += SRCDIR=\\\"$$PWD/\\\"

#DEFINES += QPLOG_USE_FUNC_INFO
DEFINES += QPLOG_USE_COLORS
DEFINES += QPLOG_USE_BENCHMARK
DEFINES += QPLOG_USE_FILE_LINE
DEFINES += QPLOG_USE_GUI_ABORT

HEADERS += \
    testerhelper.h \
    ../src/qplog.h
