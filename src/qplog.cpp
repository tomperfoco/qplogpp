/*
Copyright (c) 2014 by Tomasz Foltyński <tom@perfoco.pl>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all
copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include "qplog.h"
#include <QFile>
#include <QTime>
#include <QMutexLocker>
#include <QStringBuilder>
// for operators
#include <QPointF>
#include <QSizeF>
#include <QRectF>
#ifdef QPLOG_USE_GUI_ABORT
# include <QMessageBox>
#endif

#include <iostream> // for some critical situation, when logger can not work correctly

class QPLogStream;

void qPlogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    switch(type)
    {
        case QtDebugMsg:
            qPdebug() << "QT::" << context.file << "@" << context.line << context.function << msg;
            break;
        case QtWarningMsg:
            qPwarning() << "QT::" << context.file << "@" << context.line << context.function << msg;
            break;
        case QtCriticalMsg:
            qPcritical() << "QT::" << context.file << "@" << context.line << context.function << msg;
            break;
        case QtFatalMsg:
            qPfatal() << "QT::" << context.file << "@" << context.line << context.function << msg;
            break;
    }
}

QPLog::QPLog()
{
    s_textStream.reset(new QTextStream(stderr)); // default it is stderr
}

QByteArray QPLog::levelToString(LOGLEVEL level){
    switch(level)
    {
        case QPL_INFO:
            return QPL_INFO_STR;
        case QPL_DEBUG:
            return QPL_DEBUG_STR;
        case QPL_TRACE:
            return QPL_TRACE_STR;
        case QPL_WARNING:
            return QPL_WARNING_STR;
        case QPL_ERROR:
            return QPL_ERROR_STR;
        case QPL_CRITICAL:
            return QPL_CRITICAL_STR;
        case QPL_FATAL:
            return QPL_FATAL_STR;
        case QPL_DISABLED:
        default:
            return EMPTY_STR;
    }
    return EMPTY_STR;
}
void QPLog::addDate()
{
    (*s_textStream) << QDateTime::currentDateTime().toString(Qt::ISODate) << '\n';
    s_textStream->flush();
}
void QPLog::add(const QByteArray &log, LOGLEVEL level)
{
//    if (level != QPL_DISABLED)
//        // context uses level disabled to disable log message.
//        // levels is check ealier, in wrap makro.
//    {
        QMutexLocker lock(s_mutex.data());
        (*s_textStream) << QTime::currentTime().toString("hh:mm:ss.zzz")
                        << ' ' << levelToString(level) << log << '\n';
        s_textStream->flush();
#ifdef QPLOG_USE_GUI_ABORT
        s_logsCopy << QString(QTime::currentTime().toString("hh:mm:ss.zzz") % ' ' % levelToString(level) % log);
        if(s_logsCopy.size() > 1000)
        {
            s_logsCopy.removeFirst();
        }
#endif
//    }

    if (s_abortLevel & level)
    {
#ifdef QPLOG_USE_GUI_ABORT
        s_logsCopy.prepend("Now is " %QDate::currentDate().toString(Qt::ISODate));
        QMessageBox::critical(0, s_error_title,
                              "<body>" +
                              s_error_message +
                              "\n<a href=\"mailto:" + s_error_mailTo +
                              "?subject=" + s_error_subject +
                              "&amp;body=" + s_logsCopy.join('\n') +
                              "\">" +
                              s_error_description + "</a></body>");
#endif
#ifdef QPLOG_USE_ABORT
# ifndef Q_OS_WIN
    ::abort();
# else
    // TODO check behavior on windows for throw exception
    // calling abort() in windows makes application freeze
    ::exit(1);
# endif
#endif
    }
}

void QPLog::setContextOnly(const char *context)
{
    s_contexts.clear();
    s_contexts << context;
    s_contextType = CONTEXTS_ONLY;
}
bool QPLog::isContextOnly()
{
    return s_contextType == CONTEXTS_ONLY;
}
void QPLog::setContextAvoid(const char *context)
{
    if(s_contextType == CONTEXTS_ONLY)
    {
        s_contexts.clear();
    }
    s_contexts << context;
    s_contextType = CONTEXTS_AVOID;
}
bool QPLog::isContextAvoid()
{
    return s_contextType == CONTEXTS_AVOID;
}
void QPLog::setContextMark()
{
    s_markContext = true;
}
bool QPLog::isContextMark()
{
    return s_markContext;
}
bool QPLog::isContext(const QByteArray &context)
{
    return s_contexts.contains(context);
}
QPLogStream QPLog::debug()
{
    return QPLogStream(QPL_DEBUG);
}

QPLogStream QPLog::info()
{
    return QPLogStream(QPL_INFO);
}

QPLogStream QPLog::trace()
{
    return QPLogStream(QPL_TRACE);
}

QPLogStream QPLog::warning()
{
    return QPLogStream(QPL_WARNING);
}

QPLogStream QPLog::error()
{
    return QPLogStream(QPL_ERROR);
}

QPLogStream QPLog::critical()
{
    return QPLogStream(QPL_CRITICAL);
}

QPLogStream QPLog::fatal()
{
    return QPLogStream(QPL_FATAL);
}

void QPLog::setLogLevel(LOGLEVEL level)
{
    s_level = level;
}

void QPLog::setFilePath(const QString &path)
{
    s_file.reset(new QFile(path));
    if(s_file->exists() == true)
    {
        s_file->open(QIODevice::ReadOnly);
        QDateTime date = QDateTime::fromString(s_file->readLine(), Qt::ISODate);
        if(date.isValid() == true)
        {
            s_file->close();
            int dotPos(path.lastIndexOf("."));
            QString suffix(path.right(dotPos));
            QString baseName(path.left(path.size() - dotPos));
            std::cerr << baseName.toLatin1().constData() << std::endl;
            std::cerr << suffix.toLatin1().constData() << std::endl;
            std::cerr << s_file->rename(baseName % "_" % date.toString(Qt::ISODate) + suffix);
            s_file->setFileName(path);
        }
    }
    if(s_file->open(QIODevice::Append) == true)
    {
        QMutexLocker lock(s_mutex.data());
        s_textStream->setDevice(s_file.data());
        addDate();
    }
}

void QPLog::setStreamOutput(QTextStream *stream)
{
    QMutexLocker lock(s_mutex.data());
    s_textStream.reset(stream);
    addDate();
}
void QPLog::cleanConfiguration()
{
    s_level = QPL_DISABLED;
    s_contexts.clear();
    s_markContext = false;
    s_contextType = CONTEXTS_UNSET;
    s_abortLevel = QPLog::QPL_FATAL;
}

QPLog::LOGLEVEL QPLog::level()
{
    return s_level;
}

void QPLog::setAbortLevel(LOGLEVEL level)
{
    s_abortLevel = level;
}
void QPLog::setErrorDialogTitle(const QString &title)
{
    s_error_title = title;
}
void QPLog::setErrorDialogMessage(const QString &message)
{
    s_error_message = message;
}
void QPLog::setErrorDialogMailTo(const QString &mailTo)
{
    s_error_mailTo = mailTo;
}
void QPLog::setErrorDialogSubject(const QString &subject)
{
    s_error_subject = subject;
}
void QPLog::setErrorDialogDescription(const QString &description)
{
    s_error_description = description;
}

QString QPLog::s_error_title;
QString QPLog::s_error_message;
QString QPLog::s_error_mailTo;
QString QPLog::s_error_subject;
QStringList QPLog::s_logsCopy;
QString QPLog::s_error_description;
QScopedPointer<QMutex> QPLog::s_mutex(new QMutex);
QScopedPointer<QTextStream> QPLog::s_textStream(new QTextStream(stderr));
QScopedPointer<QFile> QPLog::s_file;
QPLog::LOGLEVEL QPLog::s_level = QPLog::QPL_DISABLED;
QPLog::LOGLEVEL QPLog::s_abortLevel = QPLog::QPL_FATAL;
QList<QByteArray> QPLog::s_contexts;
bool QPLog::s_markContext = false;
QPLog::QPL_CONTEXTS QPLog::s_contextType = QPLog::CONTEXTS_UNSET; // only avoid or only

const QByteArray QPLog::QPL_DEBUG_STR    = "DBG";
const QByteArray QPLog::EMPTY_STR    = "";
#ifndef QPLOG_USE_COLORS
const QByteArray QPLog::QPL_INFO_STR     = "INF";
const QByteArray QPLog::QPL_TRACE_STR    = "TRC";
const QByteArray QPLog::QPL_WARNING_STR  = "WRN";
const QByteArray QPLog::QPL_ERROR_STR    = "ERR";
const QByteArray QPLog::QPL_CRITICAL_STR = "CRT";
const QByteArray QPLog::QPL_FATAL_STR    = "FTL";
#else
const QByteArray QPLog::QPL_INFO_STR     = "\033[32mINF\033[0m";
const QByteArray QPLog::QPL_TRACE_STR    = "\033[34mTRC\033[0m";
const QByteArray QPLog::QPL_WARNING_STR  = "\033[36mWRN\033[0m";
const QByteArray QPLog::QPL_ERROR_STR    = "\033[33mERR\033[0m";
const QByteArray QPLog::QPL_CRITICAL_STR = "\033[35mCRT\033[0m";
const QByteArray QPLog::QPL_FATAL_STR    = "\033[31mFTL\033[0m";
#endif

QPLogStream::QPLogStream(QPLog::LOGLEVEL level)
{
    currentLevel = level;
}
QPLogStream::~QPLogStream()
{
    if(QPLog::isContextAvoid() == true && QPLog::isContext(currentContext) == true)
    {
        return;
    }
    if(QPLog::isContextOnly() == true && QPLog::isContext(currentContext) == false)
    {
        return;
    }

#ifdef QPLOG_USE_COLORS
    if(text.contains("\033[3"))
    {
        // close color
        text.append("\033[0m");
    }
#endif
    QPLog::add(text, currentLevel);
}

QPLogStream& QPLogStream::operator<<(const QString &com)
{
    text.append(" ");
    text.append(com.toLatin1()); // FIXME: lost non ascii data
    return *this;
}

QPLogStream& QPLogStream::operator<<(const char * com)
{
    text.append(" ");
    text.append(com);
    return *this;
}
//QPLogStream& QPLogStream::operator<<(const QVariant &com)
//{
//    text.append(" ");
//    text.append(com.toByteArray());
//    return *this;
//}
QPLogStream& QPLogStream::operator<<(int i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(double d)
{
    text.append(" ");
    text.append(QByteArray::number(d, 'f', 6));
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QByteArray &com)
{
    text.append(" ");
    text.append(com);
    return *this;
}
QPLogStream& QPLogStream::operator<<(quint64 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(qint64 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(qint8 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(quint8 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(qint16 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(quint16 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(quint32 i)
{
    text.append(" ");
    text.append(QByteArray::number(i));
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QPoint &p)
{
    text.append(" ");
    text.append("QPoint{");
    text.append(QByteArray::number(p.x()));
    text.append(",");
    text.append(QByteArray::number(p.y()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QPointF &p)
{
    text.append(" ");
    text.append("QPointF{");
    text.append(QByteArray::number(p.x()));
    text.append(",");
    text.append(QByteArray::number(p.y()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QSize &s)
{
    text.append(" ");
    text.append("QSize{");
    text.append(QByteArray::number(s.width()));
    text.append(",");
    text.append(QByteArray::number(s.height()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QSizeF &s)
{
    text.append(" ");
    text.append("QSizeF{");
    text.append(QByteArray::number(s.width()));
    text.append(",");
    text.append(QByteArray::number(s.height()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QRect &r)
{
    text.append(" ");
    text.append("QRect{");
    text.append(QByteArray::number(r.x()));
    text.append(",");
    text.append(QByteArray::number(r.y()));
    text.append(",");
    text.append(QByteArray::number(r.width()));
    text.append(",");
    text.append(QByteArray::number(r.height()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QRectF &r)
{
    text.append(" ");
    text.append("QRectF{");
    text.append(QByteArray::number(r.x()));
    text.append(",");
    text.append(QByteArray::number(r.y()));
    text.append(",");
    text.append(QByteArray::number(r.width()));
    text.append(",");
    text.append(QByteArray::number(r.height()));
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QTime &t)
{
    text.append(" ");
    text.append(t.toString(Qt::ISODate));
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QDateTime &t)
{
    text.append(" ");
    text.append(t.toString(Qt::ISODate));
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QDate &d)
{
    text.append(" ");
    text.append(d.toString(Qt::ISODate));
    return *this;
}
QPLogStream& QPLogStream::operator<<(bool b)
{
    text.append(" ");
    text.append(b ? "True" : "False");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const char ch)
{
    text.append(' ');
    text.append(ch);
    return *this;
}
QPLogStream& QPLogStream::operator>>(const QByteArray &context)
{
    if(currentContext.isEmpty() == false)
    {
        std::cerr << "You can not have two contexts in one log line.";
        abort();
        ::exit(1);
    }
    else if(QPLog::isContextMark() == true)
    {
        text.append(" #");
        text.append(context);
        text.append("#");
    }
    currentContext = context;
    return *this;
}

#ifdef QPLOG_USE_COLORS
QPLogStream& QPLogStream::operator<<(QPLog::COLORS color)
{
    // obsolete - use operator% instead of this
    return operator>>(color);
}
QPLogStream& QPLogStream::operator>>(QPLog::COLORS color)
{
    // todo only for linux
    switch(color)
    {
        case QPLog::RED:
            text.append("\033[31m");
            break;
        case QPLog::GREEN:
            text.append("\033[32m");
            break;
        case QPLog::YELLOW:
            text.append("\033[33m");
            break;
        case QPLog::BLUE:
            text.append("\033[34m");
            break;
        case QPLog::MAGENTA:
            text.append("\033[35m");
            break;
        case QPLog::CYAN:
            text.append("\033[36m");
            break;
        case QPLog::COLORRESET:
        default:
            text.append("\033[0m");
            break;
    }
    return *this;
}
QPLogStream& QPLogStream::operator>>(int color)
{
    return operator>>(QPLog::COLORS(color));
}
#endif
QPLogStream& QPLogStream::operator<<(const QList<QByteArray> &list)
{
    text.append(" ");
    text.append("QList<QByteArray>{");
    for(int i = 0; i < list.size(); i++)
    {
        text.append(list.at(i));
        if (list.size() > 0 && i < list.size() - 1)
        {
            text.append(",");
        }
    }
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QStringList &list)
{
    text.append(" ");
    text.append("QList{");
    for(int i = 0; i < list.size(); i++)
    {
        text.append(list.at(i));
        if (list.size() > 0 && i < list.size() - 1)
        {
            text.append(",");
        }
    }
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QList<int> &list)
{
    text.append(" ");
    text.append("QList{");
    for(int i = 0; i < list.size(); i++)
    {
        text.append(QByteArray::number(list.at(i)));
        if (list.size() > 0 && i < list.size() - 1)
        {
            text.append(",");
        }
    }
    text.append("}");
    return *this;
}
QPLogStream& QPLogStream::operator<<(const QList<QPoint> &list)
{
    text.append(" ");
    text.append("QList{");
    for(int i = 0; i < list.size(); i++)
    {
        text.append("QPoint{");
        text.append(QByteArray::number(list[i].x()));
        text.append(",");
        text.append(QByteArray::number(list[i].y()));
        text.append("}");
        if (list.size() > 0 && i < list.size() - 1)
        {
            text.append(",");
        }
    }
    text.append("}");
    return *this;
}

#ifdef QPLOG_USE_BENCHMARK
int QPRuntimeBenchmark::s_color = 0;
QPRuntimeBenchmark::QPRuntimeBenchmark(const char *name)
    : m_name(name)
{
    m_time.start();
# ifdef QPLOG_USE_COLORS
    if (++s_color > 6)
    {
        s_color = 1;
    }
    m_color = s_color;
    QPLog::debug() << m_name << ("\033[3" + QByteArray::number(m_color) + "m") << "Begining..." << "\033[0m";
# else
    QPLog::debug() << m_name << "Begining...";
# endif
}
QPRuntimeBenchmark::~QPRuntimeBenchmark()
{
# ifdef QPLOG_USE_COLORS
    QPLog::debug() << m_name << ("\033[3" + QByteArray::number(m_color) + "m") << "Ending... time elapsed:"
                   << m_time.elapsed() << "msecs" << "\033[0m";
# else
    QPLog::debug() << m_name << "Ending... time elapsed:"
                   << m_time.elapsed() << "msecs";
# endif
}
void QPRuntimeBenchmark::checkpoint(const char *str)
{
# ifdef QPLOG_USE_COLORS
    QPLog::debug() << m_name << ("\033[3" + QByteArray::number(m_color) + "m") << "CheckPoint" << str
                   << "Time elapsed:" << m_time.elapsed() << "msecs" << "\033[0m";
# else
    QPLog::debug() << m_name << "CheckPoint" << str
                   << "Time elapsed:" << m_time.elapsed() << "msecs";
# endif
}
#endif // QPLOG_USE_BENCHMARK

#ifdef QPLOG_USE_COMPARER
QPComparer::QPComparer(const QByteArray &firstData)
{
    m_data = firstData;
}
void QPComparer::compare(const QByteArray &someData)
{
    if (m_data != someData)
    {
        qPcritical() << "Data not match";
    }
}
#endif
