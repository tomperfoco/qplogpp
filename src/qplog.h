/*
Copyright (c) 2014 by Tomasz Foltyński <tom@perfoco.pl>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all
copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef QPLOG_H
#define QPLOG_H

#include <QByteArray>
#include <QTextStream>
#ifdef QPLOG_USE_BENCHMARK
#include <QMap>
#include <QTime>
#endif

class QPLogStream;
class QFile;
class QMutex;

void qPlogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

class QPLog
{
public:
    enum LOGLEVELS
    {
        QPL_DISABLED   = 0x00,
        QPL_INFO       = 0x01,
        QPL_DEBUG      = 0x02,
        QPL_TRACE      = 0x04,
        QPL_WARNING    = 0x08,
        QPL_ERROR      = 0x10,
        QPL_CRITICAL   = 0x20,
        QPL_FATAL      = 0x40,
        QPL_ALL            = 0xff
    };
    enum COLORS
    {
        // TODO - bold text
        COLORRESET,
        RED,
        GREEN,
        YELLOW,
        BLUE,
        MAGENTA,
        CYAN
    };
    enum QPL_CONTEXTS
    {
        CONTEXTS_UNSET,
        CONTEXTS_AVOID,
        CONTEXTS_ONLY,
        CONTEXTS_MARK
    };
    Q_DECLARE_FLAGS(LOGLEVEL, LOGLEVELS)
protected:
    QPLog();

    static QByteArray levelToString(LOGLEVEL level);
protected:
    static const QByteArray QPL_INFO_STR;
    static const QByteArray QPL_DEBUG_STR;
    static const QByteArray QPL_TRACE_STR;
    static const QByteArray QPL_WARNING_STR;
    static const QByteArray QPL_ERROR_STR;
    static const QByteArray QPL_CRITICAL_STR;
    static const QByteArray QPL_FATAL_STR;
    static const QByteArray EMPTY_STR;
public:
    static void add(const QByteArray &log, LOGLEVEL level);
    static void setContextOnly(const char *context);
    static bool isContextOnly();
    static void setContextAvoid(const char *context);
    static bool isContextAvoid();
    static void setContextMark();
    static bool isContextMark();
    static bool isContext(const QByteArray &context);

    static QPLogStream debug();
    static QPLogStream info();
    static QPLogStream trace();
    static QPLogStream warning();
    static QPLogStream error();
    static QPLogStream critical();
    static QPLogStream fatal();
    static void setLogLevel(LOGLEVEL level);
    static void setFilePath(const QString &path);
    static void setStreamOutput(QTextStream *stream);
    static void cleanConfiguration();
    static void setAbortLevel(LOGLEVEL level);
    static void setErrorDialogTitle(const QString &);
    static void setErrorDialogMessage(const QString &message);
    static void setErrorDialogMailTo(const QString &mailTo);
    static void setErrorDialogSubject(const QString &subject);
    static void setErrorDialogDescription(const QString &description);
    static LOGLEVEL level();

protected:
    static void addDate();

protected:
    static LOGLEVEL s_level;
    static QScopedPointer<QTextStream> s_textStream;
    static QScopedPointer<QFile> s_file;
    static QScopedPointer<QMutex> s_mutex;
    static QList<QByteArray> s_contexts;
    static bool s_markContext;
    static QPL_CONTEXTS s_contextType; // avoid or only
    static LOGLEVEL s_abortLevel;
    static QString s_error_title;
    static QString s_error_message;
    static QString s_error_mailTo;
    static QString s_error_subject;
    static QStringList s_logsCopy;
    static QString s_error_description;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QPLog::LOGLEVEL)

class QPLogStream
{
public:
    QPLogStream(QPLog::LOGLEVEL level);
    ~QPLogStream();

    QPLogStream& operator<<(const QString &com);
//    QPLogStream& operator<<(const QVariant &com); do not use this, because it
    QPLogStream& operator<<(const char * com);
    QPLogStream& operator<<(int i);
    QPLogStream& operator<<(double d);
    QPLogStream& operator<<(const QByteArray &com);
    QPLogStream& operator<<(quint64 i);
    QPLogStream& operator<<(qint64 i);
    QPLogStream& operator<<(qint8 i);
    QPLogStream& operator<<(quint8 i);
    QPLogStream& operator<<(qint16 i);
    QPLogStream& operator<<(quint16 i);
    QPLogStream& operator<<(quint32 i);
    QPLogStream& operator<<(const QPoint &p);
    QPLogStream& operator<<(const QPointF &p);
    QPLogStream& operator<<(const QSize &s);
    QPLogStream& operator<<(const QSizeF &s);
    QPLogStream& operator<<(const QRect &r);
    QPLogStream& operator<<(const QRectF &r);
    QPLogStream& operator<<(const QTime &t);
    QPLogStream& operator<<(const QDateTime &t);
    QPLogStream& operator<<(const QDate &d);
    QPLogStream& operator<<(bool b);
    QPLogStream& operator<<(const char ch);
    QPLogStream& operator>>(const QByteArray &context);
#ifdef QPLOG_USE_COLORS
    QPLogStream& operator<<(QPLog::COLORS);
    QPLogStream& operator>>(QPLog::COLORS color);
    QPLogStream& operator>>(int color);
#endif
    // put your datatypes here
    QPLogStream& operator<<(const QList<QByteArray> &list);
    QPLogStream& operator<<(const QStringList &list);
    QPLogStream& operator<<(const QList<int> &list);
    QPLogStream& operator<<(const QList<QPoint> &list);

protected:
    QByteArray text;
    QByteArray currentContext;
    QPLog::LOGLEVEL currentLevel;
};

// sanity check
#ifdef qPinfo
# error "qPinfo is already defined"
#endif
#ifdef qPdebug
# error "qPdebug is already defined"
#endif
#ifdef qPtrace
# error "qPtrace is already defined"
#endif
#ifdef qPwarning
# error "qPwarning is already defined"
#endif
#ifdef qPerror
# error "qPerror is already defined"
#endif
#ifdef qPcritical
# error "qPcritical is already defined"
#endif
#ifdef qPfatal
# error "qPfatal is already defined"
#endif

#ifdef QPLOG_USE_FUNC_INFO
# define qPdebug() \
    if ((QPLog::level() & QPLog::QPL_DEBUG) != QPLog::QPL_DEBUG) {} \
    else QPLog::debug() << Q_FUNC_INFO
# define qPinfo() \
    if ((QPLog::level() & QPLog::QPL_INFO) != QPLog::QPL_INFO) {} \
    else QPLog::info() << Q_FUNC_INFO
# define qPtrace() \
    if ((QPLog::level() & QPLog::QPL_TRACE) != QPLog::QPL_TRACE) {} \
    else QPLog::trace() << Q_FUNC_INFO
# define qPwarning() \
    if ((QPLog::level() & QPLog::QPL_WARNING) != QPLog::QPL_WARNING) {} \
    else QPLog::warning() << Q_FUNC_INFO
# define qPerror() \
    if ((QPLog::level() & QPLog::QPL_ERROR) != QPLog::QPL_ERROR) {} \
    else QPLog::error() << Q_FUNC_INFO
# define qPcritical() \
    if ((QPLog::level() & QPLog::QPL_CRITICAL) != QPLog::QPL_CRITICAL) {} \
    else QPLog::critical() << Q_FUNC_INFO
# define qPfatal() \
    if ((QPLog::level() & QPLog::QPL_FATAL) != QPLog::QPL_FATAL) {} \
    else QPLog::fatal() << Q_FUNC_INFO
#elif QPLOG_USE_FILE_LINE
# define qPdebug() \
    if ((QPLog::level() & QPLog::QPL_DEBUG) != QPLog::QPL_DEBUG) {} \
    else QPLog::debug() << qPfileline(__FILE__, __LINE__)
# define qPinfo() \
    if ((QPLog::level() & QPLog::QPL_INFO) != QPLog::QPL_INFO) {} \
    else QPLog::info() << qPfileline(__FILE__, __LINE__)
# define qPtrace() \
    if ((QPLog::level() & QPLog::QPL_TRACE) != QPLog::QPL_TRACE) {} \
    else QPLog::trace() << qPfileline(__FILE__, __LINE__)
# define qPwarning() \
    if ((QPLog::level() & QPLog::QPL_WARNING) != QPLog::QPL_WARNING) {} \
    else QPLog::warning() << qPfileline(__FILE__, __LINE__)
# define qPerror() \
    if ((QPLog::level() & QPLog::QPL_ERROR) != QPLog::QPL_ERROR) {} \
    else QPLog::error() << qPfileline(__FILE__, __LINE__)
# define qPcritical() \
    if ((QPLog::level() & QPLog::QPL_CRITICAL) != QPLog::QPL_CRITICAL) {} \
    else QPLog::critical() << qPfileline(__FILE__, __LINE__)
# define qPfatal() \
    if ((QPLog::level() & QPLog::QPL_FATAL) != QPLog::QPL_FATAL) {} \
    else QPLog::fatal() << qPfileline(__FILE__, __LINE__)
#else
# define qPdebug() \
    if ((QPLog::level() & QPLog::QPL_DEBUG) != QPLog::QPL_DEBUG) {} \
    else QPLog::debug()
# define qPinfo() \
    if ((QPLog::level() & QPLog::QPL_INFO) != QPLog::QPL_INFO) {} \
    else QPLog::info()
# define qPtrace() \
    if ((QPLog::level() & QPLog::QPL_TRACE) != QPLog::QPL_TRACE) {} \
    else QPLog::trace()
# define qPwarning() \
    if ((QPLog::level() & QPLog::QPL_WARNING) != QPLog::QPL_WARNING) {} \
    else QPLog::warning()
# define qPerror() \
    if ((QPLog::level() & QPLog::QPL_ERROR) != QPLog::QPL_ERROR) {} \
    else QPLog::error()
# define qPcritical() \
    if ((QPLog::level() & QPLog::QPL_CRITICAL) != QPLog::QPL_CRITICAL) {} \
    else QPLog::critical()
# define qPfatal() \
    if ((QPLog::level() & QPLog::QPL_FATAL) != QPLog::QPL_FATAL) {} \
    else QPLog::fatal()
#endif

#ifdef QPLOG_USE_BENCHMARK
// benchmark class
class QPRuntimeBenchmark
{
public:
    QPRuntimeBenchmark(const char *name);
    ~QPRuntimeBenchmark();
    void checkpoint(const char *str);
protected:
    QTime m_time;
//    // TODO make thread safe
//    // TODO indentation
//    static QMap<quint64, int> s_thread2indent; // TODO
# ifdef QPLOG_USE_COLORS
    static int s_color;
    int m_color;
# endif
    const char * m_name;
};
#endif

#ifdef QPLOG_USE_COMPARER
// comparer class
class QPComparer
{
public:
    QPComparer(const QByteArray &firstData);
    void compare(const QByteArray &firstData);
protected:
    QByteArray m_data;
};
#endif

// usefull macros
/*!
 * \macro       qPfileline(x, y)
 *
 * \brief       This macro creates pretty __FILE__@__LINE__ string.
 *
 * \param[in]   x - it should be __FILE__
 * \param[in]   y - it should be __LINE__
 *
 */
#ifdef qPfileline
# error "qPfileline is already defined"
#endif

#define qPfileline(x, y) \
    (x "@" + QByteArray::number(y))

/*!
 * \macro       qPSafeAssert(x)
 *
 * \brief       Prints critical message when argument is false.
 *              If QPLOG_USE_ABORT is defined, and abort level is set to
 *              QPLOG::QPL_CRITICAL this macro may abort program.
 *              This makro may be usefull for checks QObject::connect()
 *              function
 *
 * \param[in]   x - some instruction witch returs bool
 *
 */
#ifdef qPSafeAssert
#error "qPSafeAssert is already defined"
#endif

#if defined(QPLOG_USE_FILE_LINE) || defined(QPLOG_USE_FUNC_INFO)
# define qPSafeAssert(x) \
    if (x) {} \
        else qPcritical() << "REFUSED!";
#else
# define qPSafeAssert(x) \
    if (x) {} \
        else qPcritical() << qPfileline(__FILE__, __LINE__) << "REFUSED!";
#endif

#ifdef QPLOG_USE_BENCHMARK

# ifdef qPBenchmark
#  error "qPBenchmark is already defined"
# endif

# define qPBenchmark(info) \
        QPRuntimeBenchmark qpbenchmark(info);

# ifdef qPBenchmarkCheckpoint
#   error "qPBenchmarkCheckpoint is already defined"
# endif

# define qPBenchmarkCheckpoint(x) \
    qpbenchmark.checkpoint(x);

#else
# define qPBenchmark(x) {}
# define qPBenchmarkCheckpoint(x) {}
#endif // QPLOG_USE_BENCHMARK


#ifdef QPLOG_USE_COMPARER
# ifdef qPStartCompare
# error "qPStartCompare is already defined"
# endif

# define qPStartCompare(x) \
    QPComparer qpcomparer(x);

# ifdef qPCheckCompare
# error "qPCheckCompare is already defined"
# endif

# define qPCheckCompare(x) \
    qpcomparer.compare(x);
#endif

#endif // QPLOG_H
