#include "chooser.h"
#include "ui_chooser.h"
#include "qplog.h"

Chooser::Chooser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Chooser)
{
    ui->setupUi(this);
}

Chooser::~Chooser()
{
    delete ui;
}

void Chooser::setAsLoggerLevels()
{
    ui->comboBox->addItem("DISABLED", (int)(QPLog::QPL_DISABLED));
    ui->comboBox->addItem("Without DEBUG & WARNING", (int)(QPLog::QPL_INFO | QPLog::QPL_TRACE | QPLog::QPL_ERROR | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("Without DEBUG", (int)(QPLog::QPL_INFO | QPLog::QPL_TRACE | QPLog::QPL_ERROR | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL | QPLog::QPL_WARNING));
    ui->comboBox->addItem("WARNING & ERROR & CRITICAL & FATAL", (int)(QPLog::QPL_WARNING | QPLog::QPL_ERROR | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("INFO & CRITICAL & FATAL", (int)(QPLog::QPL_INFO | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("FATAL", (int)(QPLog::QPL_FATAL));
    ui->comboBox->addItem("DEBUG", (int)(QPLog::QPL_DEBUG));
    ui->comboBox->addItem("ALL", (int)(QPLog::QPL_ALL));
}
QVariant Chooser::getCurrentItemData()
{
    return ui->comboBox->currentData();
}
void Chooser::setAsAbortLevels()
{
    ui->comboBox->addItem("DISABLED", (int)(QPLog::QPL_DISABLED));
    ui->comboBox->addItem("Without DEBUG & INFO & TRACE", (int)(QPLog::QPL_WARNING | QPLog::QPL_ERROR | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("ERROR & CRITICAL & FATAL", (int)(QPLog::QPL_ERROR | QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("CRITICAL & FATAL", (int)(QPLog::QPL_CRITICAL | QPLog::QPL_FATAL));
    ui->comboBox->addItem("FATAL", (int)(QPLog::QPL_FATAL));
}
void Chooser::setAsContexts()
{
    ui->comboBox->addItem("Right mouse button", "RMB");
    ui->comboBox->addItem("Left mouse button", "LMB");
    ui->comboBox->addItem("Keyboard", "Keyboard");
}
