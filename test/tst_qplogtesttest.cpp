/*
Copyright (c) 2014 by Tomasz Foltyński <tom@perfoco.pl>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all
copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/*!
*   \file       qplogtesttest.cpp
*
*   \brief      Test and benchmark file for qplogpp
*
**/

#include <QString>
#include <QtTest>
#ifndef QPLOG_USE_GUI_ABORT
# include <QCoreApplication>
#else
# include <QApplication>
#endif

#include "testerhelper.h"

#include "../src/qplog.h"

#include <iostream>

class QPLogTestTest : public QObject
{
    Q_OBJECT
public:
    QPLogTestTest();

private Q_SLOTS:
    void testVariousQPlog();
    void testOperatorsQPlog();
    void testContextOnlyQPlog();
    void testContextMarkQPlog();
    void testContextAvoidQPlog();
    void testContextMarkAndAvoidQPlog();
    void testContextMarkAndOnlyQPlog();
    void testQPBenchmark();
    // todo - other tests
};

QPLogTestTest::QPLogTestTest()
{
    QPLog::setFilePath("testlog.log");
}

void QPLogTestTest::testVariousQPlog()
{
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_DEBUG |
                       QPLog::QPL_CRITICAL |
                       QPLog::QPL_WARNING |
                       QPLog::QPL_ERROR);
    // info and trace should be ommited
    QBENCHMARK
    {
        qPwarning() << "some integer:" << 10
                    << QString("very very very very very very very very very very")
                    << QString("very very very very very very very very very very")
                    << QString("very very very very long log text")
                    << QString("very very very very very very very very very very")
                    << QString("very very very very very very very very very very")
                    << QString("very very very very long log text")
                    << "some double" << 3.14 << QString("This is test string");
                    ;
        qPcritical() << "some integer:" << 10
                     << QString("very very very very very very very very very very")
                     << QString("very very very very very very very very very very")
                     << QString("very very very very long log text")
                     << QString("very very very very very very very very very very")
                     << QString("very very very very very very very very very very")
                     << QString("very very very very long log text")
                     << "some double" << 3.14 << QString("This is test string")
                     ;
        qPinfo() << "some integer:" << 10
                 << QString("very very very very very very very very very very")
                 << QString("very very very very very very very very very very")
                 << QString("very very very very long log text")
                 << QString("very very very very very very very very very very")
                 << QString("very very very very very very very very very very")
                 << QString("very very very very long log text")
                 << "some double" << 3.14 << QString("This is test string")
                 ;
        qPtrace() << "some integer:" << 10
                  << QString("very very very very very very very very very very")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very long log text")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very long log text")
                  << "some double" << 3.14 << QString("This is test string")
                  ;
        qPerror() << "some integer:" << 10
                  << QString("very very very very very very very very very very")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very long log text")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very very very very very very very")
                  << QString("very very very very long log text")
                  << "some double" << 3.14 << QString("This is test string")
                  ;
    }
}
void QPLogTestTest::testOperatorsQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_DEBUG);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // Create tested objects.
    QPoint point(2,3);
    QPointF pointf(2.33,.43);
    QSize size(3,4);
    QSizeF sizef(2.44,.54);
    QRect rect(1,2,3,4);
    QRectF rectf(1.1,2.2,3.3,4.4);
    QStringList stringList;
    stringList << "One";
    stringList << "Two";
    stringList << "Three";
    QList<int> intList;
    intList << 1;
    intList << 2;
    intList << 3;
    intList << 4;
    intList << 5;
    QList<QPoint> pointList;
    pointList << QPoint(1,2);
    pointList << QPoint(2,3);
    pointList << QPoint(3,4);
    pointList << QPoint(4,5);
    pointList << QPoint(5,6);
    QTime time(QTime::currentTime());
    QDateTime dateTime(QDateTime::currentDateTime());
    QDate date(QDate::currentDate());
    // add here other common qt types

    // and test it
    qPdebug() << "QPoint" << point;
    QVERIFY(testArray.endsWith("QPoint QPoint{2,3}\n"));
    qPdebug() << "QPointF" << pointf;
    QVERIFY(testArray.endsWith("QPointF QPointF{2.33,0.43}\n"));
    qPdebug() << "QSize" << size;
    QVERIFY(testArray.endsWith("QSize QSize{3,4}\n"));
    qPdebug() << "QSizeF" << sizef;
    QVERIFY(testArray.endsWith("QSizeF QSizeF{2.44,0.54}\n"));
    qPdebug() << "QRect" << rect;
    QVERIFY(testArray.endsWith("QRect QRect{1,2,3,4}\n"));
    qPdebug() << "QRectF" << rectf;
    QVERIFY(testArray.endsWith("QRectF QRectF{1.1,2.2,3.3,4.4}\n"));
    qPdebug() << "QStringList" << stringList;
    QVERIFY(testArray.endsWith("QStringList QList{One,Two,Three}\n"));
    qPdebug() << "QList<int>" << intList;
    QVERIFY(testArray.endsWith("QList<int> QList{1,2,3,4,5}\n"));
    qPdebug() << "QList<QPoint>" << pointList;
    QVERIFY(testArray.endsWith("QList<QPoint> QList{QPoint{1,2},QPoint{2,3},QPoint{3,4},QPoint{4,5},QPoint{5,6}}\n"));
    qPdebug() << "QTime" << time;
    QVERIFY(testArray.endsWith(("QTime " + time.toString(Qt::ISODate) + "\n").toLatin1()));
    qPdebug() << "QDateTime" << dateTime;
    QVERIFY(testArray.endsWith(("QDateTime " + dateTime.toString(Qt::ISODate) + "\n").toLatin1()));
    qPdebug() << "QDate" << date;
    QVERIFY(testArray.endsWith(("QDate " + date.toString(Qt::ISODate) + "\n").toLatin1()));
}
void QPLogTestTest::testContextOnlyQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_DEBUG);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // set logger to print only context
    QPLog::setContextOnly("only");

    qPdebug() >> "avoid" << "C_AVOID";
    qPdebug() >> "mark" << "C_MARK";
    qPdebug() >> "only" << "C_ONLY";

    QVERIFY(testArray.contains("C_AVOID") == false);
    QVERIFY(testArray.contains("avoid") == false);
    QVERIFY(testArray.contains("C_MARK") == false);
    QVERIFY(testArray.contains("mark") == false);
    QVERIFY(testArray.contains("C_ONLY") == true);
    QVERIFY(testArray.contains("only") == false);
}
void QPLogTestTest::testContextMarkQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_DEBUG);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // set logger to mark context
    QPLog::setContextMark();

    qPdebug() >> "avoid" << "C_AVOID";
    qPdebug() >> "mark" << "C_MARK";
    qPdebug() >> "only" << "C_ONLY";
    QVERIFY(testArray.contains("C_AVOID") == true);
    QVERIFY(testArray.contains("avoid") == true);
    QVERIFY(testArray.contains("C_MARK") == true);
    QVERIFY(testArray.contains("mark") == true);
    QVERIFY(testArray.contains("C_ONLY") == true);
    QVERIFY(testArray.contains("only") == true);
}
void QPLogTestTest::testContextAvoidQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_ALL);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // set logger to avoid context
    QPLog::setContextAvoid("avoid");

    qPdebug() >> "avoid" << "C_AVOID";
    qPdebug() >> "mark" << "C_MARK";
    qPdebug() >> "only" << "C_ONLY";
    QVERIFY(testArray.contains("C_AVOID") == false);
    QVERIFY(testArray.contains("avoid") == false);
    QVERIFY(testArray.contains("C_MARK") == true);
    QVERIFY(testArray.contains("mark") == false);
    QVERIFY(testArray.contains("C_ONLY") == true);
    QVERIFY(testArray.contains("only") == false);
}
void QPLogTestTest::testContextMarkAndAvoidQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_ALL);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // set logger to avoid and mark context
    QPLog::setContextAvoid("avoid");
    QPLog::setContextMark();

    qPdebug() >> "avoid" << "C_AVOID";
    qPdebug() >> "mark" << "C_MARK";
    qPdebug() >> "only" << "C_ONLY";

    QVERIFY(testArray.contains("C_AVOID") == false);
    QVERIFY(testArray.contains("avoid") == false);
    QVERIFY(testArray.contains("C_MARK") == true);
    QVERIFY(testArray.contains("mark") == true);
    QVERIFY(testArray.contains("C_ONLY") == true);
    QVERIFY(testArray.contains("only") == true);
}
void QPLogTestTest::testContextMarkAndOnlyQPlog()
{
    // logger settings
    QPLog::cleanConfiguration();
    QPLog::setLogLevel(QPLog::QPL_ALL);
    QByteArray testArray;
    QTextStream *stream(new QTextStream(&testArray));
    QPLog::setStreamOutput(stream);

    // set logger to print only and mark context
    QPLog::setContextOnly("only");
    QPLog::setContextMark();

    qPdebug() >> "avoid" << "C_AVOID";
    qPdebug() >> "mark" << "C_MARK";
    qPdebug() >> "only" << "C_ONLY";
    QVERIFY(testArray.contains("C_AVOID") == false);
    QVERIFY(testArray.contains("avoid") == false);
    QVERIFY(testArray.contains("C_MARK") == false);
    QVERIFY(testArray.contains("mark") == false);
    QVERIFY(testArray.contains("C_ONLY") == true);
    QVERIFY(testArray.contains("only") == true);
}
void QPLogTestTest::testQPBenchmark()
{
#ifdef QPLOG_USE_BENCHMARK
    QPLog::cleanConfiguration();
    QTextStream *stream = new QTextStream(stderr);
    // note that delete is automatically calls from QScopedPointer.
    QPLog::setStreamOutput(stream);
    QPLog::setLogLevel(QPLog::QPL_ALL);
    TesterHelper helper;
    helper.oneLevelBenchmark();
#else
    QSKIP("QPLOG_USE_BENCHMARK not defined.");
#endif
}

int main(int argc, char *argv[])
{
#ifndef QPLOG_USE_GUI_ABORT
    QCoreApplication app(argc, argv); // create application object for some features
#else
    QApplication app(argc, argv); // create application object for some features
#endif
    QPLogTestTest test;
    return QTest::qExec(&test, argc, argv);
}

#include "tst_qplogtesttest.moc"
