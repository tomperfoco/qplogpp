#-------------------------------------------------
#
# Project created by QtCreator 2015-05-03T20:49:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GuiTestApp
TEMPLATE = app

INCLUDEPATH += ../../../qplogpp/src/\

SOURCES += main.cpp\
        mainwindow.cpp \
        chooser.cpp \
        ../../../qplogpp/src/qplog.cpp \

HEADERS  += mainwindow.h \
    chooser.h

FORMS    += mainwindow.ui \
    chooser.ui

DEFINES += QPLOG_USE_ABORT
#DEFINES += QPLOG_USE_COLORS
DEFINES += QPLOG_USE_GUI_ABORT
