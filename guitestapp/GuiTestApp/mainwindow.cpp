/*
Copyright (c) 2014 by Tomasz Foltyński <tom@perfoco.pl>

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all
copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/*!
*   \file       mainwindow.cpp
*   \brief      Simple application for tests qplog gui features.
**/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMouseEvent>
#include <qplog.h>
#include "chooser.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->inputWidget->installEventFilter(this);

    QPLog::setContextMark();
    QPLog::setErrorDialogMailTo("tofol80@gmail.com");
    QPLog::setErrorDialogTitle("Critical error occurred.");
    QPLog::setErrorDialogSubject("Test app error.");
    QPLog::setErrorDialogDescription("Click to send logs by mail.");
    QPLog::setErrorDialogMessage("Application caught critical error. Please send logs by e-mail, to help us prevent this situation in future.");

    // silent mode example - if GUI ABORT is enabled, log will be pass to email, when abort level entered.
    QByteArray *ba(new QByteArray());
    QTextStream *stream(new QTextStream(ba));
    QPLog::setStreamOutput(stream);

    // log to file example
//    QPLog::setFilePath("test.log");
}
MainWindow::~MainWindow()
{
    delete ui;
}
/*!
 * \brief Read logs from other logger. Open file for reading and read it up to date.
 */
void MainWindow::on_pb_chooseInputFile_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Choose some logfile to simulate input.", "/var/log");
    // TODO create file input reader class.
    qPtrace() >> "GUI" << "choose file path" << filePath;
    qPcritical() << "Not implemented yet!";
}
/*!
 * \brief Ask user for new log levels, and set it.
 */
void MainWindow::on_pb_setLevels_clicked()
{
    Chooser chooser;
    chooser.setAsLoggerLevels();
    if(chooser.exec() == QDialog::Rejected)
    {
        return;
    }
    else
    {
        QPLog::setLogLevel(QPLog::LOGLEVELS(chooser.getCurrentItemData().toInt()));
    }
}
void MainWindow::on_pb_setAbortLevel_clicked()
{
    Chooser chooser;
    chooser.setAsAbortLevels();
    if(chooser.exec() == QDialog::Rejected)
    {
        return;
    }
    else
    {
        QPLog::setAbortLevel(QPLog::LOGLEVELS(chooser.getCurrentItemData().toInt()));
    }
}

void MainWindow::on_pb_setContextOnly_clicked()
{
    Chooser chooser;
    chooser.setAsContexts();
    if(chooser.exec() == QDialog::Accepted)
    {
        QPLog::setContextOnly(chooser.getCurrentItemData().toString().toLatin1().constData());
    }
}
void MainWindow::on_pb_setContextAvoid_clicked()
{
    Chooser chooser;
    chooser.setAsContexts();
    if(chooser.exec() == QDialog::Accepted)
    {
        QPLog::setContextAvoid(chooser.getCurrentItemData().toString().toLatin1().constData());
    }
}
bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        qPdebug() >> "Keyboard" << "key pressed" << keyEvent->key();
        return true;
    }
    else if(event->type() == QEvent::MouseMove)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        qPdebug() << "mouse move" << mouseEvent->pos();
        return true;
    }
    else if(event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        QByteArray ctx;
        if(mouseEvent->button() == Qt::RightButton)
        {
            ctx = "RMB";
        }
        else if(mouseEvent->button() == Qt::LeftButton)
        {
            ctx = "LMB";
        }
        qPwarning() >> ctx.constData() << "mouse button press" << mouseEvent->pos();
        return true;
    }
    else if(event->type() == QEvent::MouseButtonRelease)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        QByteArray ctx;
        if(mouseEvent->button() == Qt::RightButton)
        {
            ctx = "RMB";
        }
        else if(mouseEvent->button() == Qt::LeftButton)
        {
            ctx = "LMB";
        }
        qPtrace() >> ctx.constData() << "mouse button release" << mouseEvent->pos();
        return true;
    }
    else if(event->type() == QEvent::MouseButtonDblClick)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        QByteArray ctx;
        if(mouseEvent->button() == Qt::RightButton)
        {
            ctx = "RMB";
        }
        else if(mouseEvent->button() == Qt::LeftButton)
        {
            ctx = "LMB";
        }
        qPinfo() >> ctx.constData() << "mouse button double click" << mouseEvent->pos();
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }
}
